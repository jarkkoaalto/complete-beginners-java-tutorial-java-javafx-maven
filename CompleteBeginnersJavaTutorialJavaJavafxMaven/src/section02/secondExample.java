package section02;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class secondExample extends Application{

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Button btn = new Button("Click Me");
		Button exit = new Button("Exit");
		
		exit.setOnAction(e -> {
			System.out.println("Hello, World!");
			System.exit(0);
		});
		
		
		VBox root = new VBox();
		root.getChildren().add(btn); //  Or call .addall(btn, exit);
		root.getChildren().add(exit); //
		Scene scene = new Scene(root,500,300);
		primaryStage.setTitle("My Title");
		primaryStage.setScene(scene);
		
		 primaryStage.show();
		
	}

}
