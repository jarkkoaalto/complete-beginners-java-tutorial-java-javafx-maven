package section01;
/*
 * The finally block always executes when the try block exits.
 * This ensures that the finally block is executed even if an unexpected execption occurs.
 */
public class TryCatchDemo {
	
	
	public static int retInt() {
		int a = 100;
		try {
			return a + 1;
		}
			// System.exit(1); Only situation when finally block don't called
 		
		catch(ArithmeticException ax) {
			System.out.println(ax);
			return a;
		} finally {
			//a = 1000;
			System.out.println("Finally called");
			//return a;
		}
		
	}
	

	public static void main(String [] args) {
		System.out.println(retInt());
 	
		
	}
}
