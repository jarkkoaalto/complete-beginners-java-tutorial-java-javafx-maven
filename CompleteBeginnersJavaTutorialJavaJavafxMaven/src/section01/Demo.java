package section01;

class MyThread extends Thread{

	@Override
	public void run() {
		for(int i=0; i<10; i++) {
			System.out.println(Thread.currentThread().getId() + " Value " + i);
		}
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

public class Demo {

	public static void main(String[] args) {
		MyThread myClass = new MyThread();
		myClass.start();
		
		MyThread myClass1 = new MyThread();
		myClass1.start();
	}
}
