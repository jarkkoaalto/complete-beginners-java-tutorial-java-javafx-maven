package section01;

import java.util.Scanner;



public class GettingUserInput {

	public static void main(String[] args) {
	
		Scanner lukija = new Scanner(System.in);
		System.out.println("Enter some number : ");
		int userInputNumber = lukija.nextInt();
		
		Scanner kirjain = new Scanner(System.in);
		System.out.println("Enter some text : ");
		String userInputString = kirjain.nextLine();
		
		
		System.out.println("The entered value is : " + userInputNumber);
		System.out.println("The entered string is : " + userInputString);
	}

}
