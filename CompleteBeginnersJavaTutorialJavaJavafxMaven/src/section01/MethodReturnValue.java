package section01;

public class MethodReturnValue {

	public static void main(String[] args) {
		
		sayHello("Jarkko");
		sayHello("Matti");
		sayHello("Tommi");
		
		
		Add(4,5);
		Add(5334,55);
		Add(887,234);
		int sum = Add(234,445,10);
		System.out.println(sum);
		int result = sum * 25;
		System.out.println(result);
	}

	public static void sayHello(String name) {
		System.out.println("Hello " + name);
	}
	
	public static void Add(int a, int b) {
		System.out.println(a+b);
	}
	public static int Add(int a, int b, int c) {
		return (a+b+c);
	}
}
