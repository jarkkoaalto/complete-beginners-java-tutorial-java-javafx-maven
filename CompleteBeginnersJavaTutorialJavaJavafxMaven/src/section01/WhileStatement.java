package section01;

public class WhileStatement {

	public static void main(String[] args) {
		int a = 0;
		int b = 10;
		
		while(a <= 10){
			System.out.println("T�m� on (" + a + ") kertaa kivaa!");
			a++;
		}

		System.out.println(" ");
		
		while(b >= 10){
			System.out.println("T�m� on (" + b + ") kertaa kivaa");
			b--;
		}
		
		System.out.println(" ");
		
		while(b>0){
			System.out.println(b);
			b--;
		}
	}

}
