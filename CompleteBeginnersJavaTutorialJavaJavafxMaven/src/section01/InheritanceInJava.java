package section01;

public class InheritanceInJava {

	/*
	 * Classes in Java can be extends
	 * creating new classes which retain
	 * characterisics of the base class
	 * 
	 */
	public static void main(String[] args) {
		Rectangle rec = new Rectangle();
		Triangle tri = new Triangle();
		
		rec.set_Values(10, 20);
		tri.set_Values(10, 20);
		
		System.out.println("Area of Rectangle = " +  rec.area());
		System.out.println("Area of Triangle = " + tri.area());
	}

}
