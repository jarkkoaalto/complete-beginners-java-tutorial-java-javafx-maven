package section01;

import java.util.ArrayList;
import java.util.LinkedList;

public class DifferenceBetweenArrayAndLinkedListDemo {

	public static void main(String[] args) {
		/*
		 * 1) LinkedList element Insertion is faster compared to ArrayList
		 * 
		 * 2) Arraylist search operation is pretty fast compared to the LinkedList
		 * 	search operation.
		 * 
		 * 3)LinkedList element deletion is faster compared to ArrayList
		 * 
		 * 4) LinkedList has more memory overhead than ArrayList because
		 * in ArrayList each index only holds actual object
		 * but in case of LinkedList each node holds both data and address of next 
		 * and previous node.
		 */
		long n = (long) 1E7;
		
		ArrayList arraylist = new ArrayList();
		long milis = System.currentTimeMillis();
		
		for(int i = 0; i < n; i++) {
			arraylist.add(i);
		}
		System.out.println("Insert arraylist takes " + (System.currentTimeMillis()));
		
		LinkedList linkedlist = new LinkedList();
		milis = System.currentTimeMillis();
		for(int i=0; i<n; i++) {
			linkedlist.add(i);
		}
		System.out.println("Insert linkedlist takes " + (System.currentTimeMillis()) + " ms");
		
		milis = System.currentTimeMillis();
		arraylist.remove(1E5);
		System.out.println("del front arraylist takes " + (System.currentTimeMillis()));
		
		milis = System.currentTimeMillis();
		arraylist.remove(0);
		System.out.println("del front linkedlist takes " + (System.currentTimeMillis()) + "ms");
	
		milis = System.currentTimeMillis();
		linkedlist.get(0);
		System.out.println("get front linkedlist takes " + (System.currentTimeMillis()));
		
		// get from front , mid, end
		milis = System.currentTimeMillis();
		arraylist.remove((int)n/2);
		System.out.println("del front arraylist takes " + (System.currentTimeMillis()) + "ms");
	
		milis = System.currentTimeMillis();
		linkedlist.get((int)n/2);
		System.out.println("get front linkedlist takes " + (System.currentTimeMillis()));
	}

}
