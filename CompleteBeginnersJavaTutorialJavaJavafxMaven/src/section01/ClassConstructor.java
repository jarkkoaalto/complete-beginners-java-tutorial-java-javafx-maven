package section01;

public class ClassConstructor {

	public static void main(String[] args) {
		Gube cube1 = new Gube();
		System.out.println(cube1.getCubeVolume());
		
		Gube cube2 = new Gube(20,20,20);
		System.out.println(cube2.getCubeVolume());

	}

}
