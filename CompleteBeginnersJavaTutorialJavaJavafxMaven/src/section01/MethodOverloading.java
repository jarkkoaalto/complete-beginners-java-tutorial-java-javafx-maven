package section01;

public class MethodOverloading {

	public static void main(String[] args) {
	System.out.println("Integer: " + Add(1,36));
	System.out.println("Double: "+ Add(2.0, 5.0));
	System.out.println("String: " + Add("Hello", " world"));

	}
	
	public static int Add(int a, int b) {
		return(a+b);		
	}
	
	public static double Add(double a, double b) {
		return(a+b);
	}
	
	public static String Add(String a, String b) {
		return(a+b);		
	}
	
	

}
