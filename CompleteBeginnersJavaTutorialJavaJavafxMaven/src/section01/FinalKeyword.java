package section01;

/*
 * Final keyword has a numerious way to use:
 * 
 * A final class cannot be subcalssed
 * A final method cannot be overridden by subclasses
 * A final variable can only be initialized once
 */

public class FinalKeyword extends Opiskelija {
	public final int number ;
	
	FinalKeyword() {
		number = 10;
	}
	
	
	public static void main(String [] args) {
		FinalKeyword fk = new FinalKeyword();
		
	}

}
