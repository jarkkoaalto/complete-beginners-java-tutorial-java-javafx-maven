package section01;

public class StaticKeyWord {

	// What does the 'static' keyword do in a class?
	public static void main(String[] args) {
		// static members belongs to the class instead of a specific instance
		Hello hello = new Hello();
		hello.age = 18;
		System.out.println(hello.age);
		// Hello.DoSomething("Hi youtube");
		// hello.DoSomethingElse("Hello world");
		
		Hello hello1 = new Hello();
		hello1.age=23;
		System.out.println(hello1.age );
	}
	
	
}
 
