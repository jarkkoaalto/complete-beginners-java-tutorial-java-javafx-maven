package section01;

public class ForLoops {

	public static void main(String[] args) {
		int[] myintarray = {100,31,26,48,52};
		
		for(int i = 0; i<5 ; i++){
			System.out.print(myintarray[i] + " ");
		}
		System.out.println(" ");
		
		for(int element : myintarray) {
			System.out.print(element + " ");		
		}
	}
}
