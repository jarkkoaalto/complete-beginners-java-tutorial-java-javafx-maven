package section01;

import java.util.LinkedList;

public class LinkedListDemo {
	
	public static void main(String [] args) {
		LinkedList<String> name = new LinkedList<String>();
		name.add("Jarkko");
		name.add("Mark");
		name.add("Tom");
		name.add("John");
		
		name.addFirst("Jack");
		name.addLast("Zack");
		
		name.add(1, "Tom");
		
		System.out.println(name.size());
		for(String x : name) {
			System.out.println(x);
		}
	}

}
