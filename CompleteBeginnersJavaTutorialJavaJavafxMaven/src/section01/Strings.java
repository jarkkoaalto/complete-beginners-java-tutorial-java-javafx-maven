package section01;

public class Strings {

	public static void main(String[] args) {
		String myString = "Hello World !";
		String myStringLowCase = myString.toLowerCase();
		String myStringUpCase = myString.toUpperCase();
		
		
		System.out.println(myString);
		System.out.println(myString.length()); // palauttaa merkkijonon pituuden
		System.out.println(myStringLowCase);
		System.out.println(myStringUpCase);
		
	// Replace any character
		System.out.println(myString.replace('e', 'a'));
		
		System.out.println(myString.indexOf('o'));
	}

}
