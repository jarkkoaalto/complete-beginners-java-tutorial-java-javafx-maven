package section01;

import java.util.*;

/*
 * public boolean hasNext();
 * public Object next();
 * public boolean hasPrevious();
 * public Object previous();
 * 
 */

public class ListIteratorsDemo {

	public static void main(String[] args) {
		ArrayList<String> names = new ArrayList<String>();
		names.add("Mark");
		names.add("Tom");
		names.add("John");
		names.add("Jack");
		names.add("Patric");
		
		ListIterator<String> iter = names.listIterator();
		while(iter.hasNext())
			System.out.println(iter.next());
		
		System.out.println(" ");
		
		while(iter.hasPrevious())
			System.out.println(iter.previous());
			

	}

}
