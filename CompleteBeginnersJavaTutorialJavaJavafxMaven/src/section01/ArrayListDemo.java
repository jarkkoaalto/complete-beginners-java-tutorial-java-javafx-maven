package section01;

import java.util.ArrayList;

public class ArrayListDemo {

	public static void main(String[] args) {
		int simple_array[] = new int[5];// or {1,2,3,4,5}*/

		ArrayList<Integer> myList = new ArrayList<Integer>(5);
		myList.add(1);
		myList.add(5);
		myList.add(9);
		
		
		for(int x :myList) {
			System.out.println(x);
		}
		
		System.out.println("size = " + myList.size());
		myList.remove(2);
		myList.trimToSize();
		
		for(int x:myList) {
			System.out.println(x);
		}
		System.out.println("size = " + myList.size());
	}

}
