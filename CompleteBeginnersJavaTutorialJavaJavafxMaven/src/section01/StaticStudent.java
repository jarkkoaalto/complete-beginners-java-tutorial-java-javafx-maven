package section01;

public class StaticStudent {
	// What does the 'static' keyword do in a class?
	public static void main(String[] args) {
		Student mark = new Student();
		System.out.println(mark.getNoOfStudents());
		Student tom = new Student();
		System.out.println(tom.getNoOfStudents());
		
		System.out.println(Student.getNoOfStudents());

	}

}
