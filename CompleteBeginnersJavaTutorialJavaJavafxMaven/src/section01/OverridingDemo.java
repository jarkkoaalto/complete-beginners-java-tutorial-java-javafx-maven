package section01;

public class OverridingDemo {

	public static void main(String []args) {
		BankDemo abc = new Bank_ABC();
		BankDemo asd = new Bank_DEF();
		BankDemo qwer = new Bank_XYZ();
		
		System.out.println(asd.getInterestRate());
		System.out.println(qwer.getInterestRate());
		System.out.println(abc.getInterestRate());
	}
}
