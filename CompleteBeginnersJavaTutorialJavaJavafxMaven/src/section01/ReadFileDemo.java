package section01;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFileDemo {

	public static void main(String[] args) {
		BufferedReader br = null;
		try {
			//br = new BufferedReader(new FileReader("C:\\Users\\Jakko\\Desktop\\Read.txt"));
			br = new BufferedReader(new FileReader("Read.txt"));
			String line;
			
			while((line = br.readLine()) != null) {
				System.out.println(line);
			}
			
		}catch(IOException e) {
			System.out.println();
		}finally {
			try {
				br.close();
			} catch (IOException e) {
			
				e.printStackTrace();
				System.out.println("Something gone frong");
			}
			
		}

	}

}
