package section01;

public class SwitchStatement {
	
	public static void main(String [] args){
		int score = 90;
		// can use: byte, short, int or char.
		
		
		switch(score){
			case 90:
				System.out.println("Very Good");
				break;
			case 80:
				System.out.println("Good");
				break;
			case 60:
				System.out.println("Ok");
				break;
			case 50:
				System.out.println("Poor ");
				break;
			case 40:
				System.out.println("Fail");
				break;
			default:
				System.out.println("Out of range");
				break;
		}
	}

}
