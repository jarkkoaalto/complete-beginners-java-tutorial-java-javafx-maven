package section01;

import java.util.*;

public class HasSetDemo {

	public static void main(String[] args) {
		HashSet<String> name = new HashSet<String>();
		name.add("Mia");
		name.add("Mark");
		name.add("Matti");
		name.add("Miia");
		name.add("Minna");
		name.add("Mio");

		Iterator<String> iter = name.iterator();
		while(iter.hasNext()) {
			System.out.println(iter.next()); 	
		}
		
		
	
		System.out.println(iter);
	}

}
