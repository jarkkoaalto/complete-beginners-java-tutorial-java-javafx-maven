package section01;

/*
 * if(boolean_expression)
 * {
 * 		execute if the boolean expression is true
 * }
 * 
 * 
 * Comparison operators:
 * 
 * 	== is equals to
 *  != is not equal to
 *  > is greater than
 *  < is less than
 *  >= is greater than or equal to
 *  <= is less than or equal to
 */

public class IfElse {
	public static void main(String [] args) {
		int x = 10;
		if(x == 10) {
			System.out.println("yes x == 10");
		}else{
			System.out.println("No x == 10 => it is " + x);
		}
	}

}
