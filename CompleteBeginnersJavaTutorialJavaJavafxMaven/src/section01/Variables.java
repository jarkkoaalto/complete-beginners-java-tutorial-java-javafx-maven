package section01;

public class Variables {
	
	public static void main(String [] args) {
		
		/*
		 * Variables
		 * 
		 * - byte (number, 1 byte)
		 * - short (number, 2 bytes)
		 * - int (number, 4 bytes)
		 * - long (number, 8 bytes)
		 * - float (float number, 4 bytes)
		 * - double (float number, 8 bytes)
		 * - char (a character, 2 bytes)
		 * - boolean (true or false, 1 byte)
		 * 
		 */
		float my_float = 4.5f;
		double my_double = 3.55;
		int my_variable  = 10;
		char my_char = 'A';
		boolean my_boolean = true;
		
		System.out.println(my_variable);
		System.out.println(my_float);
		System.out.println(my_double);
		System.out.println(my_char);
		System.out.println(my_boolean);
	}

}
